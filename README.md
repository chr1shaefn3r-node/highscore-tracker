# Highscore Tracker

>> Mit dem Highscore Tracker kannst du deine Spieler und Punkte waehrend des Spiels verwalten.

Mit dem Highscore Tracker du die Punkte deiner Spieler verwalten.
Die Punkte werden automatisch verrechnet und der Highscore Tracker liest dir am Ende vor wer gewonnen hat.

## Beginnen

 * Mit Highscore Tracker sprechen
 * Den Highscore Tracker starten
 * Ich will meine Punkte zaehlen
 * Wir wollen eine Runde Karten spielen

## Theme

 * Primary Color: #DD1A1E

## Privacy Policy

[Privacy Policy](https://gitlab.christophhaefner.de/node/highscore-tracker/blob/master/PrivacyPolicy.md)

