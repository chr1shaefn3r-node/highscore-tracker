set -x;
set -e;
docker-compose -p "$JOB_NAME-prod" -f docker-compose-prod.yml stop
# -v => Remove volumes associated with containers
docker-compose -p "$JOB_NAME-prod" -f docker-compose-prod.yml rm --force -v
docker-compose -p "$JOB_NAME-prod" -f docker-compose-prod.yml up -d --force-recreate
docker-compose -p "$JOB_NAME-prod" -f docker-compose-prod.yml ps

