'use strict';

// =================================================================================
// App Configuration
// =================================================================================

const {App} = require('jovo-framework');

const config = {
    logging: true,
};

const app = new App(config);


// =================================================================================
// App Logic
// =================================================================================

function getScoreboard(game) {
    console.log("Calculating scoreboard from: ", game.scores);
    return Object.entries(game.scores)
      .sort(([player, score], [secondPlayer, secondScore]) => {
          return secondScore - score;
      })
      .map(([name, score]) => {
        return {name, score};
      });
}

const GAMES = {};

app.setHandler({
    'LAUNCH': function() {
        this.toIntent('HighscoreTrackerGame');
    },

    'HighscoreTrackerGame': function() {
        console.log("HighscoreTrackerGame")
        console.log({userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        if (typeof GAMES[this.getUserId()] === "undefined") {
            GAMES[this.getUserId()] = {
                players: [],
                scores: {}
            };
            this.ask('Spiel wurde angelegt');
        } else {
            this.ask('Spiel wird fortgesetzt');
        }
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'AddPlayer': function(name) {
        const playerName = name.value.toLowerCase();
        console.log("AddPlayer")
        console.log({name: playerName, userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        const game = GAMES[this.getUserId()];
        if (game.players.includes(playerName)) {
            this.ask(playerName + ' hab ich schon notiert!');
            return;
        }
        game.players.push(playerName);
        game.scores[playerName] = 0;
        this.ask('Willkommen ' + playerName + ', hast ja doch keine Chance!');
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'WhoIsPlaying': function() {
        console.log("WhoIsPlaying")
        console.log({userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        const { players } = GAMES[this.getUserId()];
        this.ask(`Es spielen mit: ${players.join(',')}`);
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'AddScore': function(name, punkte) {
        console.log("AddScore")
        if (typeof name === "undefined" || typeof name.value === "undefined" || typeof punkte === "undefined" || typeof punkte.key === "undefined") {
            this.ask(`Ich hab dich nicht verstanden.`);
            return;
        }
        const playerName = name.value.toLowerCase();
        console.log({name: playerName, punkte: punkte.key, userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        const { players, scores } = GAMES[this.getUserId()];
        if (!players.includes(playerName)) {
            this.ask(playerName + ' ist nicht Teil des Spiels!');
            return;
        }
        scores[playerName] += punkte.key;
        this.ask(`Hab ich notiert.`);
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'AddNegativeScore': function(name, punkte) {
        console.log("AddNegativeScore")
        if (typeof name === "undefined" || typeof name.value === "undefined" || typeof punkte === "undefined" || typeof punkte.key === "undefined") {
            this.ask(`Ich hab dich nicht verstanden.`);
            return;
        }
        const playerName = name.value.toLowerCase();
        console.log({name: playerName, punkte: punkte.key, userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        const { players, scores } = GAMES[this.getUserId()];
        if (!players.includes(playerName)) {
            this.ask(playerName + ' ist nicht Teil des Spiels!');
            return;
        }
        scores[playerName] -= punkte.key;
        this.ask(`Hab ich notiert.`);
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'Scoreboard': function() {
        console.log("Scoreboard")
        console.log({userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        const scoreboard = getScoreboard(GAMES[this.getUserId()]);
        console.log(scoreboard);
        this.ask(`Spieler ${scoreboard[0].name} führt das Spiel mit ${scoreboard[0].score} an.
              Es folgen ${scoreboard.slice(1).map(player => `${player.name} mit ${player.score}`).join(",")}`);
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'FinishGame': function() {
        console.log("FinishGame")
        console.log({userId: this.getUserId(), gameStateBefore: GAMES[this.getUserId()]})
        GAMES[this.getUserId()] = {};
        this.tell("Schade ich will weiter spielen");
        console.log({userId: this.getUserId(), gameStateAfter: GAMES[this.getUserId()]})
    },

    'Default Fallback Intent': function(name) {
        this.ask('Ob du behindert bist?');
    },
});

module.exports.app = app;

