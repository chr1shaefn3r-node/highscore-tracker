# Privacy Policy

Christoph Haefner, Fabian Schwarz-Fritz, Sebastian Schneider built the Highscore Tracker app as an Open Source app. This SERVICE is provided by Christoph Häfner at no cost and is intended for use as is.

This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.

If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.

## Information Collection and Use

We don't story any personally identifieable information.
We do store names and scores during the tracked games. The information that we request will be retained by us and used as described in this privacy policy.

Link to privacy policy of third party service providers used by the app:

 * [Google Actions](https://www.google.com/policies/privacy/)

## Log Data

We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.

## Security

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

## Changes to This Privacy Policy

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

## Contact Us

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
